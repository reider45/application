package me.reider45.Rainbow.Events;

import me.reider45.Rainbow.Main;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoin implements Listener {
	
	private Main pl;
	public PlayerJoin(Main in){
		pl = in;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		pl.spawnRainbow(e.getPlayer(), 2);
	}

}
