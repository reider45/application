package me.reider45.Gadgets.Handlers;

import java.util.ArrayList;
import java.util.List;

import me.reider45.Gadgets.Weapons.Gadget;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class GadgetHandler {
	
	public static List<Gadget> gadgets = new ArrayList<Gadget>();
	
	public static Boolean isGadget(ItemStack i){
		for(Gadget g : gadgets){
			if(g.getItem().isSimilar(i)){
				return true;
			}
		}
		
		return false;
	}
	
	public static Gadget hasGadget(Player p){
		for(ItemStack i : p.getInventory().getContents()){
			if(isGadget(i)){
				return getGadget(i.getItemMeta().getDisplayName());
			}
		}
		
		return null;
	}
	
	public static Gadget getGadget(String name){
		for(Gadget g : gadgets){
			if(g.getName().equals(name)){
				return g;
			}
		}
		
		return null;
	}
	
	public static void removeGadget(Player p){
		p.getInventory().setArmorContents(null);
		
		p.getInventory().remove(Material.ARROW);
		p.getInventory().setItem(0, null);
	}
	
	public static void equipGadget(Player p, Gadget g){
		removeGadget(p);
		
		if(g.getName().equals("Bow Popper")){
			p.getInventory().setItem(9, new ItemStack(Material.ARROW, 1));
		}
		
		p.getInventory().setItem(0, g.getItem());
	}

}
