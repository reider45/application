package me.reider45.Rainbow;

import me.reider45.Rainbow.Events.PlayerJoin;
import me.reider45.Rainbow.utils.ParticleEffect; 

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin {

	public void onEnable(){
		// Register join listener to have rainbow appear
		Bukkit.getPluginManager().registerEvents(new PlayerJoin(this), this);
	}

	// Spawn rainbow
	public void spawnRainbow(final Player player, final int radius){

		final Location loc = player.getLocation();
		new BukkitRunnable(){
			int count = 0;
			public void run(){
				
				// Play the rainbow effect multiple times
				int currentColor = 6;
				for(int i = 0; i < 7; i++){
					// Spawn new wave, increasing radius and changing color
					newWave(loc, radius+i, currentColor);
					currentColor = nextColor(currentColor);
				}
				
				count++;
				if(count > 20){
					// Cancel
					this.cancel();
				}
			}
		}.runTaskTimer(this, 0, 4);

	}

	// Spawn wave of rainbow
	private void newWave(final Location l, final int radius, final int insertColor){

		new BukkitRunnable(){
			Location loc = l;
			double t = 0;

			int currentColor = insertColor;
			public void run(){
				t += Math.PI/16;
				double x = Math.cos(t) * (radius);
				double y = Math.cos(t) * radius;
				double z = Math.sin(t) * (radius);
				
				// Only play if in semi-circle
				if( x > 0 ){
					loc.add(x, y, z);
					ParticleEffect.NOTE.display( new ParticleEffect.NoteColor( currentColor ) , loc, 50);
					loc.subtract(x, y, z);
				}

				// Canceller
				if( t > (Math.PI*4)){
					this.cancel();
				}
			}

		}.runTaskTimer(this, 0, 1);

	}

	// Get next color to be used
	private Integer nextColor(Integer i){
		
		switch(i){
		case 6: // Red
			return 8;
		case 8: // Orange
			return 7;
		case 7: // Yellow
			return 22;
		case 22: // Green
			return 14;
		case 14: // Blue
			return 1;
		case 1: // Indigo
			return 15;
		case 15: // Violet
			return 0;
		}

		return 0;
	}


}
