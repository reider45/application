package me.reider45.Gadgets.Weapons;

import me.reider45.Gadgets.Main;
import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

public class GadgetBow extends Gadget implements Listener {
	
	public GadgetBow() {
		super(Material.BOW, "Bow Popper", 0);
		super.getItem().addEnchantment(Enchantment.ARROW_INFINITE, 1);
	}
	
	// Drop Arrow
		@EventHandler
		public void onDrop(PlayerDropItemEvent e){
			if(e.getItemDrop().getItemStack().getType() == Material.ARROW){
				e.setCancelled(true);
			}
		}
		
		// Shoot
		@EventHandler
		public void onFire(EntityShootBowEvent e){
			if(e.getEntity() instanceof Player){
				
				Player p = (Player)e.getEntity();
				if(GadgetHandler.getGadget("Bow Popper").getItem().isSimilar(p.getItemInHand())){
					e.setCancelled(true);
					
					Projectile proj = p.launchProjectile(Arrow.class);
					proj.setShooter(p);
					proj.getVelocity().multiply(0.5);
					proj.setMetadata("POPPER", new FixedMetadataValue(Main.getInstance(), "gadgets"));
				}
			}
		}
		
		// Hit
		@EventHandler
		public void onHit(EntityDamageByEntityEvent e){
			
			if(e.getDamager() instanceof Projectile){
				Projectile proj = (Projectile)e.getDamager();
				
				if(proj.hasMetadata("POPPER")){
					if(e.getEntity() instanceof Player){
						e.setCancelled(true);
						e.getEntity().setVelocity(new Vector(0,10,0));
					}
				}
			}
		}
		
		// Fall
		@EventHandler
		public void onFall(EntityDamageEvent e){
			if(e.getCause() == DamageCause.FALL){
				if(e.getEntity() instanceof Player){
					e.setCancelled(true);
				}
			}
		}

}
