package me.reider45.Gadgets.Weapons;

import me.reider45.Gadgets.Handlers.Cooldown;
import me.reider45.Gadgets.Handlers.CooldownHandler;
import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class GadgetPearl extends Gadget implements Listener {

	public GadgetPearl(){
		super(Material.ENDER_PEARL, "Riding Pearl", 5);
	}

	@EventHandler
	public void onShoot(PlayerInteractEvent e){
		Player p = e.getPlayer();

		if(e.getAction().name().contains("RIGHT")){
			if(p.getItemInHand() == null){ return; }

			if(GadgetHandler.isGadget(p.getItemInHand())){

				Gadget pearl = GadgetHandler.getGadget("Riding Pearl");
				if(pearl.getItem().isSimilar(p.getItemInHand())){
					e.setCancelled(true);

					// On CD still
					if(CooldownHandler.hasCooldown(p, pearl)){ return; }

					new Cooldown(pearl, p);

					// Fire
					Projectile proj = p.launchProjectile(EnderPearl.class);
					proj.setPassenger(p);

				}
			}
		}
	}

	// Cancel TP

	@EventHandler
	public void pearlTP(PlayerTeleportEvent e) {
		if (e.getCause() == TeleportCause.ENDER_PEARL){
			e.setCancelled(true);
		}
	}

}
