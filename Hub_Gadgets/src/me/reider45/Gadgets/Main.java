package me.reider45.Gadgets;

import me.reider45.Gadgets.Events.GadgetEvent;
import me.reider45.Gadgets.Events.MenuEvent;
import me.reider45.Gadgets.Events.PlayerEvent;
import me.reider45.Gadgets.Handlers.Menu;
import me.reider45.Gadgets.Weapons.GadgetBat;
import me.reider45.Gadgets.Weapons.GadgetBow;
import me.reider45.Gadgets.Weapons.GadgetDisco;
import me.reider45.Gadgets.Weapons.GadgetFlesh;
import me.reider45.Gadgets.Weapons.GadgetPearl;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {
	
	private static Main pl;
	public static Main getInstance(){
		return pl;
	}
	
	public void onEnable(){
		pl = this;
		
		Bukkit.getPluginManager().registerEvents(new GadgetEvent(), this);
		Bukkit.getPluginManager().registerEvents(new MenuEvent(), this);
		Bukkit.getPluginManager().registerEvents(new PlayerEvent(), this);
		
		Bukkit.getPluginManager().registerEvents(new GadgetBow(), this);
		Bukkit.getPluginManager().registerEvents(new GadgetBat(), this);
		Bukkit.getPluginManager().registerEvents(new GadgetFlesh(), this);
		Bukkit.getPluginManager().registerEvents(new GadgetPearl(), this);
		Bukkit.getPluginManager().registerEvents(new GadgetDisco(), this);
		
		Menu.registerInventory();
	}
	
	public void onDisable(){
		
	}

}
