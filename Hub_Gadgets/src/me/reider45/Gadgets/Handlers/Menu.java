package me.reider45.Gadgets.Handlers;

import me.reider45.Gadgets.Weapons.Gadget;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Menu {
	
	public static Inventory menuInventory;
	public static ItemStack menuItem;
	
	public static ItemStack remove;
	
	public static void registerInventory(){
		remove = new ItemStack(Material.GOLD_BLOCK, 1);
		ItemMeta meta = remove.getItemMeta();
		meta.setDisplayName("�4Remove");
		remove.setItemMeta(meta);
		
		menuItem = new ItemStack(Material.CHEST, 1);
		meta = menuItem.getItemMeta();
		meta.setDisplayName("�6Gadget Menu");
		menuItem.setItemMeta(meta);
		
		menuInventory = Bukkit.createInventory(null, 9, "Gadget Menu");
		menuInventory.setItem(8, remove);
		
		int i = 0;
		for(Gadget g : GadgetHandler.gadgets){
			menuInventory.setItem(i, g.getItem());
			i++;
		}
	}

}
