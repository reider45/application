package me.reider45.Bungee;

import me.reider45.Bungee.Commands.AddCommand;
import me.reider45.Bungee.Commands.DelCommand;
import me.reider45.Bungee.Commands.HostCommand;
import me.reider45.Bungee.Commands.SetCommand;
import net.md_5.bungee.api.plugin.Plugin;

public class Main extends Plugin {
	
	private static Main plugin;
	
	public static Main getInstance(){
		return plugin;
	}
	
	public void onEnable(){
		plugin = this;
		// Register Commands
		getProxy().getPluginManager().registerCommand(this, new AddCommand());
		getProxy().getPluginManager().registerCommand(this, new SetCommand());
		getProxy().getPluginManager().registerCommand(this, new DelCommand());
		getProxy().getPluginManager().registerCommand(this, new HostCommand());
	}

}