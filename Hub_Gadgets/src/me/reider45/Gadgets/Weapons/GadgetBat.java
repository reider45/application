package me.reider45.Gadgets.Weapons;

import java.util.ArrayList;
import java.util.List;

import me.reider45.Gadgets.Main;
import me.reider45.Gadgets.Handlers.Cooldown;
import me.reider45.Gadgets.Handlers.CooldownHandler;
import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class GadgetBat extends Gadget implements Listener {

	public GadgetBat() {
		super(Material.IRON_BARDING, "Bat Blaster", 4);
	}
	
	@EventHandler
	public void onShoot(PlayerInteractEvent e){
		Player p = e.getPlayer();
		
		if(e.getAction().name().contains("RIGHT")){
			if(p.getItemInHand() == null){ return; }
				
				if(GadgetHandler.isGadget(p.getItemInHand())){
					
					Gadget bat = GadgetHandler.getGadget("Bat Blaster");
					if(bat.getItem().isSimilar(p.getItemInHand())){
						
						// On CD still
						if(CooldownHandler.hasCooldown(p, bat)){ return; }
						
						new Cooldown(bat, p);
						
						// Fire
						
						final List<Bat> bats = new ArrayList<Bat>();
						for(int i = 0; i < 30; i ++){
							bats.add( p.getWorld().spawn(p.getEyeLocation(), Bat.class) );
						}
						
						new BukkitRunnable(){
							public void run(){
								for(Bat b : bats){
									b.remove();
								}
							}
						}.runTaskLaterAsynchronously(Main.getInstance(), 20L);
						
						
						
					}
					
				}
					
					
			
			
			
		}
		
		
	}
	
	

}
