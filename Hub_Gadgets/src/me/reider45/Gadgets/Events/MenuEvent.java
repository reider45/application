package me.reider45.Gadgets.Events;

import me.reider45.Gadgets.Handlers.GadgetHandler;
import me.reider45.Gadgets.Handlers.Menu;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class MenuEvent implements Listener {
	
	@EventHandler
	public void onOpen(PlayerInteractEvent e){
		Player p = e.getPlayer();
		if(p.getItemInHand() != null){
			
			if(p.getItemInHand().isSimilar(Menu.menuItem)){
				e.setCancelled(true);
				p.openInventory(Menu.menuInventory);
			}
			
		}
	}
	
	@EventHandler
	public void onChoose(InventoryClickEvent e){
		if(e.getInventory().getName().equals(Menu.menuInventory.getName())){
			e.setCancelled(true);
			
			ItemStack clicked = e.getCurrentItem();
			if(clicked == null){ return; }
			
			Player p = (Player)e.getWhoClicked();
			if(GadgetHandler.isGadget(clicked)){
				
				GadgetHandler.equipGadget( (Player)e.getWhoClicked(), GadgetHandler.getGadget(clicked.getItemMeta().getDisplayName()) );
				
				p.closeInventory();
			}else
				
			if(clicked.isSimilar(Menu.remove)){
				
				GadgetHandler.removeGadget(p);
				p.closeInventory();
				
			}
			
		}
	}
	
	@EventHandler
	public void onInv(InventoryClickEvent e){
		if(e.getCurrentItem() != null){
			if(e.getCurrentItem().isSimilar(Menu.menuItem) 
					|| e.getCurrentItem().getType() == Material.ARROW
					|| GadgetHandler.isGadget(e.getCurrentItem())){
				e.setCancelled(true);
			}
		}
	}


}
